// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class Layout_ToyBoxxTarget : TargetRules
{
	public Layout_ToyBoxxTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "Layout_ToyBoxx" } );
	}
}
