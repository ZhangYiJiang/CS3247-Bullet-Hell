// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class Layout_ToyBoxxEditorTarget : TargetRules
{
	public Layout_ToyBoxxEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "Layout_ToyBoxx" } );
	}
}
