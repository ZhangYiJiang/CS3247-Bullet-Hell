// Fill out your copyright notice in the Description page of Project Settings.

#include "A_ProjectilePool.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "AsyncWork.h"
#include "ParallelFor.h"

class CheckProjectileOutOfBoundsTask : public FNonAbandonableTask
{
	friend class FAutoDeleteAsyncTask<CheckProjectileOutOfBoundsTask>;

	AA_ProjectilePool* Pool;

	CheckProjectileOutOfBoundsTask(AA_ProjectilePool* ProjectilePool)
		: Pool(ProjectilePool)
	{
	}

	void DoWork()
	{
		auto MaxBound = Pool->MaxBound;
		auto MinBound = Pool->MinBound;

		for (auto Actor : Pool->UsedActors) {
			auto Location = Actor->GetActorLocation();

			if (Location.X > MaxBound.X || Location.X < MinBound.X ||
				Location.Y > MaxBound.Y || Location.Y < MinBound.Y ||
				Location.Z > MaxBound.Z || Location.Z < MinBound.Z) {

				Pool->mutex.Lock();
					Pool->ToRelease.Add(Actor);
				Pool->mutex.Unlock();
			}
		}
	}

	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(CheckProjectileOutOfBoundsTask, STATGROUP_ThreadPoolAsyncTasks);
	}
};


// Sets default values
AA_ProjectilePool::AA_ProjectilePool()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AA_ProjectilePool::BeginPlay()
{
	Super::BeginPlay();

	// Populate the pool with some actors to start with
	UWorld* world = GetWorld();

	FActorSpawnParameters spawnParams;
	spawnParams.Owner = this;
	FRotator spawnRotation(0, 0, 0);

	for (int i = 0; i < Count; i++) {
		ReleaseActorToPool(world->SpawnActor<AActor>(ProjectileClass, SpawnPoint, spawnRotation, spawnParams));
	}

	CheckDistance = (ProjectileRadius + ShipRadius) * (ProjectileRadius + ShipRadius);

	// Define max and min bounds
	MaxBound = FVector(FMath::Max(BoundA.X, BoundB.X), FMath::Max(BoundA.Y, BoundB.Y), FMath::Max(BoundA.Z, BoundB.Z));
	MinBound = FVector(FMath::Min(BoundA.X, BoundB.X), FMath::Min(BoundA.Y, BoundB.Y), FMath::Min(BoundA.Z, BoundB.Z));
}

// Called every frame
void AA_ProjectilePool::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Remove actors outside of bounds
	LastBoundCheck -= DeltaTime;
	Ticks++;

	if (LastBoundCheck < 0) {
		(new FAutoDeleteAsyncTask<CheckProjectileOutOfBoundsTask>(this))->StartBackgroundTask();
		LastBoundCheck = 0.5;
	}

	if (ToRelease.Num()) {
		mutex.Lock();
			for (auto Actor : ToRelease) {
				ReleaseActorToPool(Actor);
			}

			ToRelease.Empty();
		mutex.Unlock();
	}

	// Move projectiles - on 2s if we're more than 70fps
	if (Ticks % 2 == 0) {
		for (auto Actor : UsedActors) {
			Actor->AddActorLocalOffset(ActorVelocities[Actor] * (DeltaTime + LastTickDelta));
		}
	}

	LastTickDelta = DeltaTime;

	// Check ship bounds
	if (!Ship) return;

	auto ShipLocation = Ship->GetActorLocation();

	for (auto &Actor : UsedActors) {
		if (FVector::DistSquared(Actor->GetActorLocation(), ShipLocation) < CheckDistance) {
			HitActors.Add(Actor);
		}
	}

	for (auto &HitActor : HitActors) {
		if (HitActor && OnShipHit(HitActor)) {
			ReleaseActorToPool(HitActor);
		}
	}
}

void AA_ProjectilePool::ReleaseActorToPool(AActor* Actor) {
	ReleaseActorToPool(Actor, true);
}

void AA_ProjectilePool::ReleaseActorToPool(AActor* Actor, bool RemoveFromUsedActors)
{
	if (Actor) {
		AvailableActors.Add(Actor);

		if (RemoveFromUsedActors) {
			UsedActors.Remove(Actor);
		}

		Actor->SetActorLocation(SpawnPoint);
		Actor->SetActorHiddenInGame(true);
	}
}

AActor * AA_ProjectilePool::GetActorFromPool(FVector Velocity)
{
	AActor* Actor;

	if (AvailableActors.Num()) {
		Actor = AvailableActors.Pop(false);
	}
	else {
		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		FRotator spawnRotation(0, 0, 0);

		Actor = GetWorld()->SpawnActor<AActor>(ProjectileClass, SpawnPoint, spawnRotation, spawnParams);
	}

	verify(Actor != nullptr);

	ActorVelocities.Add(Actor, Velocity);
	Actor->SetActorHiddenInGame(false);

	UsedActors.Add(Actor);

	return Actor;
}

void AA_ProjectilePool::ReleaseAllActors()
{
	for (auto Actor : UsedActors) {
		ReleaseActorToPool(Actor, false);
	}

	UsedActors.Empty();
}
