// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "A_ProjectilePool.generated.h"

UCLASS()
class LAYOUT_TOYBOXX_API AA_ProjectilePool : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AA_ProjectilePool();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	float CheckDistance;

	float LastBoundCheck;

	float LastTickDelta;

	int32 Ticks;

	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	FVector MaxBound;
	FVector MinBound;
	FCriticalSection mutex;

	TSet<class AActor*> ToRelease;
	TSet<class AActor*> HitActors;

	UFUNCTION(BlueprintCallable)
	void ReleaseActorToPool(AActor* Actor);
	void ReleaseActorToPool(AActor * Actor, bool RemoveFromUsedActors);

	UFUNCTION(BlueprintCallable)
	AActor* GetActorFromPool(FVector Velocity);

	UFUNCTION(BlueprintImplementableEvent)
	bool OnShipHit(AActor* Projectile);

	UFUNCTION(BlueprintCallable)
	void ReleaseAllActors();

	UPROPERTY(BlueprintReadOnly)
	TArray<class AActor*> AvailableActors;

	UPROPERTY(BlueprintReadOnly)
	TSet<class AActor*> UsedActors;

	UPROPERTY()
	TMap<class AActor*, FVector> ActorVelocities;

	UPROPERTY(EditAnywhere)
	int16 Count;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class AActor> ProjectileClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (MakeEditWidget))
	FVector BoundA;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (MakeEditWidget))
	FVector BoundB;

	UPROPERTY(EditAnywhere, meta = (MakeEditWidget))
	FVector SpawnPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* Ship;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ShipRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ProjectileRadius;
};
