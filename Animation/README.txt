Currently plays at 60FPS, speed can be edited.

120-360 Idle Animation
420-520 Quick Fire Animation
720-1200 Boss Entry Animation
1260-1500 Machine Gun Animation
1560-1620 Quickfire (Rapid succession of bullet wave)
1740-1860 Enemy Spawn Animation
1920-2400 Shield Activate/Deactivate (0.5 sec activate and deactivate time;  seconds shield up time)
2460-2880 Boss Death Animation